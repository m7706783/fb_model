# Minimal computational model used for studying fragmented network bursts in Doorn et al. 2024.
# Adapted from model of neuronal network derived from hiPSCs on MEA (Doorn et al. 2023)
# Using adaptive expontential integrate and fire neuron model

# Written by Nina Doorn
# last edit: 19-02-2024
# n.doorn-1@utwente.nl

# IMPORT LIBRARIES
from brian2 import devices
import scipy
from scipy import signal
import os
from numpy import *
from numpy import random
from brian2 import *
import networkx as nx
import matplotlib.pyplot as plt
import pickle
import numpy as np
from scipy.stats import norm
from scipy.signal import find_peaks
from scipy.io import savemat

# # Skip C99 support check
import brian2.codegen.cpp_prefs
brian2.codegen.cpp_prefs._compiler_supports_c99 = True

BrianLogger.suppress_hierarchy('brian2.devices.device')
BrianLogger.suppress_hierarchy('brian2.parsing.bast.floating_point_division')

# SET PARAMETERS
# simulation parameters
outputdir = '/home/Nina/Documents/FB_project/Output'    #where do you want to save your graphs
simname = '/test'                               #name of the simulation for saving

simtime = 35 * second               # simulation time
transient = 5 * second              # time to discard as transient
devices.device.seed(139)            # set the seed for all the random number realisations

# choose with plots you want to see
voltplot = False                    # plots the membrane potential of one neuron in the network over time
electrodeplot = True                # plots the voltage measured by the 12 virtual electrodes
rasterplot = False                  # Makes a brian2 rasterplot with all the neurons
rasterlecplot = False               # Makes a rasterplot of APs detected at electrodes (electrodeplot should be True)
onechannelplot = True               # Plots the voltage measured at one electrode
STDplot = True                      # Plots short-term depression parameters over time
adaptationplot = True               # Plots the afterhyperpolarizatoin current over time
mechanismplot = True                # Plots the mechanisms underlying FBs
firingrateplot = True               # Plots the network firing rate over time

# network parameters
Nl = 10
N = Nl * Nl                         # number of neurons

# neuron parameters
C = 100 * pF                        # membrane capacitance
gL = 1.8 * nS                       # Membrane conductance (1/resistance)
taum = C / gL                       # Membranr time constant
EL = -40 * mV                       # Reversal potential of the leak current
VTL = -20 * mV                      # EIF Parameter
DeltaT = 2 * mV                     # EIF Parameter
Vcut = 20 * mV                      # Cut-off of the action potential
sigma = 8 * mV                      # how much noise every neuron receives
Vr = EL                             # Reset value

E_AHP = -80 * mV                    # reversal potential of the sAHP current
g_AHP = 100 * nS                    # maximum AHP conductance
tau_Ca = 6000 * ms                  # recovery time constant sAHP conductance
alpha_Ca = 0.00035                  # strength of the spike-frequency adaptation

# synapse parameters
g_syn = 6 * nS                      # synaptic strength
tau_syn = 20 * ms                   # synaptic recovery time constant
E_syn = 0 * mV                      # reversal potential of synaptic current
prob = 0.1                          # connection probability of neurons
S = 3                               # synaptic weight scaling parameter

#Short-Term Depression parameters
tau_d = 813 * ms                   # Recovery time constant of synaptic depression
U = 0.2                            # Magnitude of depression

# BUILD NETWORK
# neuron model
eqs = Equations('''
dV/dt = (gL*(EL - V) + gL*DeltaT*exp((V - VTL)/DeltaT) + I - I_syn + I_AHP)/C + sigma*sqrt(2/taum)*xi : volt (unless refractory)
I_AHP = -g_AHP*Ca*(V-E_AHP) : amp
dCa/dt = - Ca / tau_Ca : 1 
I_syn =  g_syn*(V-E_syn)*s_syn : amp
ds_syn/dt = -s_syn/tau_syn :1
dx_d/dt = (1-x_d)/tau_d :1
x : meter
y : meter
I : amp
''')

# Make population of neurons
P = NeuronGroup(N, model=eqs, threshold='V>Vcut',
                reset="V=Vr; Ca += alpha_Ca; x_d *= (1-U)", refractory=2 * ms, method='euler')

# Initialize neuron parameters
P.V = EL                                # approximately resting membrane potential
P.I = '(rand() -0.5)* 35 * pA'          # Make neurons heterogeneously excitable
P.x_d = 1

# Position neurons on a grid
grid_dist = 45 * umeter
P.x = '(i % Nl) * grid_dist'
P.y = '(i // Nl) * grid_dist'

# synapse model
eqs_onpre = '''
s_syn += S * x_d 
'''

# Make synapses
Conn = Synapses(P, P, on_pre=eqs_onpre, method='euler')
Conn.connect(p=prob)

# SET UP MONITORS AND RUN
recordstring = ['V']
if STDplot:
    recordstring.extend(['I_syn', 'x_d'])

if adaptationplot:
    recordstring.append('I_AHP')

dt2 = defaultclock.dt                                           # Allows for chanching the timestep of recording

trace = StateMonitor(P, recordstring, record=True, dt=dt2)

spikes = SpikeMonitor(P)
run(simtime, report='text', profile=True)                       # run

# PLOT
if voltplot:
    plt.figure(dpi=200)
    plot(trace.t / second, trace[9].V / mV, 'k', linewidth=0.7)
    xlabel('time (s)')
    ylabel('Membrane Potential of a neurons (mV)')
    savefig(outputdir + simname + 'voltsingleneuron.png')
    show()

if rasterplot:
    plt.figure(dpi=200)
    plt.plot(spikes.t / second, spikes.i, '.k', ms=0.7)
    xlabel('time (s)')
    ylabel('neuron index')
    xlim([44, 48])
    savefig(outputdir + simname + 'brianraster.png')
    show()

# set up a filter to filter the voltage signal
fs = 1 / (dt2 / second)
fc = 100                                            # Cut-off frequency of the filter
w = fc / (fs / 2)                                   # Normalize the frequency
b, a = signal.butter(5, w, 'high')

if electrodeplot:
    elec_grid_dist = (grid_dist * (Nl - 1)) / 4     # electrode grid size (there are 12 electrodes)
    elec_range = 3 * grid_dist                      # measurement range of each electrode
    comp_dist = ((Nl - 1) * grid_dist - elec_grid_dist * 3) / 2
    voltagetraces = zeros((12, len(trace.t)))

    try:
        elecranges = pickle.load(open("elecranges.dat", "rb"))
    except FileNotFoundError:
        #Find the neurons that need to be measures, outcomment the first time run.
        elecranges = np.zeros((16, 40), dtype=int)
        for i in range(len(elecranges)):
            templist = list()
            for j in range(N):
                x_electrode = i % 4 * elec_grid_dist + comp_dist
                y_electrode = i // 4 * elec_grid_dist + comp_dist
                if sqrt((x_electrode - P[j].x) ** 2 + (y_electrode - P[j].y) ** 2) < elec_range:
                    templist.append(j)
                elecranges[i, 0:len(templist)] = templist
            templist = None
        pickle.dump(elecranges, open("elecranges.dat", "wb"))

    plt.figure(figsize=(10, 6), dpi=300)
    k = 0
    MaxAPs = len(spikes.t)
    APs = np.array([0,0])
    for i in [1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14]:
        templist = elecranges[i, :]
        x_electrode = i % 4 * elec_grid_dist + comp_dist
        y_electrode = i // 4 * elec_grid_dist + comp_dist
        templist = [j for j in templist if j != 0]
        Voltage = np.zeros(len(trace.V[0]))
        for l in range(len(templist)):
            Voltage += trace[templist[l]].V / mV * 1 / (
                        sqrt((x_electrode - P.x[templist[l]]) ** 2 + (y_electrode - P.y[templist[l]]) ** 2) / (
                            grid_dist * 0.2))
        Voltage = Voltage - mean(Voltage)
        Voltagefilt = signal.filtfilt(b, a, Voltage)  # high pass filter
        voltagetraces[k, :] = Voltagefilt
        threshold = 4 * np.sqrt(np.mean(Voltagefilt ** 2))      #threshold to detect APs
        APstemp, _ = find_peaks(abs(Voltagefilt), height=threshold)
        for j in range(len(APstemp)):
            APs = np.append(APs, [k,APstemp[j]])
        plt.plot(trace.t / second, Voltagefilt + k * 70, linewidth=0.75, color='black')
        templist = None
        k += 1
    xlabel('time (s)', fontsize=15)
    plt.tick_params(
        axis='y',
        which='both',
        left=False,
        right=False,
        labelleft=False)
    #xlim([17, 19])
    tight_layout()
    plt.savefig(outputdir + simname + 'elecvolt.png')
    show()

if onechannelplot:
    figure(dpi=300)
    plot(trace.t / second, voltagetraces[2, :], 'k', linewidth=0.75)
    xlabel('time (s)')
    ylabel('Voltage (mV)')
    savefig(outputdir + simname + 'onelecvolt.png')
    show()

if rasterlecplot:
    APsres = APs.reshape(len(APs) // 2, 2)
    figure(figsize=(10, 2.2), dpi=300)
    plt.plot(APsres[:, 1] * dt2, APsres[:, 0], 'k|', ms=9, alpha=.5)
    plt.hlines([-0.3, 0.7, 1.7, 2.7, 3.7, 4.7, 5.7, 6.7, 7.7, 8.7, 9.7, 10.7], -3, simtime/second+3, colors='black',
               linewidths=0.5)
    xlabel('Time (second)')
    ylabel('Spike times per electrode')
    xlim([-1, simtime/second+1])
    #xlim([9, 41])
    axis("off")
    tight_layout()
    savefig(outputdir + simname + 'ownraster.png')
    show()

if STDplot:
    fig, axs = plt.subplots(3, 1, sharex='all', dpi=200, figsize=[8, 6.5])
    axs[0].plot(spikes.t / second, spikes.i, '.k', ms=1)
    axs[0].set_ylabel('Neuron index')

    axs[1].plot(trace.t / second, sum(trace.x_d, axis=0) / len(trace.x_d), 'k')
    axs[1].set_ylabel('Short-Term depression')
    axs[1].grid(visible=True, linestyle=':')

    axs[2].plot(trace.t / second, sum(trace.I_syn, axis=0) / len(trace.I_syn) * -1 / pA, 'k')
    axs[2].set_ylabel('Synaptic Current (pA)')
    axs[2].grid(visible=True, linestyle=':')
    axs[2].set_xlabel('time (s)')
    fig.tight_layout()
    savefig(outputdir + simname + 'STDplot.png', dpi=300)
    show()

if mechanismplot:
    fig, axs = plt.subplots(4, 1, sharex='all', dpi=200, figsize=[8, 6.5])
    axs[0].plot(spikes.t / second, spikes.i, '.k', ms=1)
    axs[0].set_ylabel('Neuron index')

    axs[1].plot(trace.t / second, sum(trace.x_d, axis=0) / len(trace.x_d), 'k')
    axs[1].set_ylabel('Short-Term depression')
    axs[1].grid(visible=True, linestyle=':')

    axs[2].plot(trace.t / second, sum(trace.I_syn, axis=0) / len(trace.I_syn) * -1 / pA, 'k')
    axs[2].set_ylabel('Synaptic Current (pA)')
    axs[2].grid(visible=True, linestyle=':')
    axs[2].set_xlabel('time (s)')

    axs[3].plot(trace.t / second, sum(trace.I_AHP, axis=0) / len(trace.I_AHP) * -1 / pA, 'k')
    axs[3].set_ylabel('I_sAHP (pA)')
    axs[3].set_xlabel('time (s)')
    axs[3].grid(visible=True, linestyle=':')
    #axs[3].set_ylim([-1, 35])
    fig.tight_layout()
    xlim([transient/second, (simtime-transient)/second])
    xlim([13, 20])
    savefig(outputdir + simname + 'mechplot.png', dpi=300)
    show()

# CALCULATE NETWORK FIRING RATE
APs = APs.reshape(len(APs) // 2, 2)

# remove transient or select certain area
start = transient
stop = simtime
APs_wot = APs[APs[:, 1] > start * fs / second, :]
APs_wot = APs_wot[APs_wot[:, 1] < stop * fs / second, :]
APs_wot[:, 1] = APs_wot[:, 1] - start * fs / second

#Maybe save the data for analysis
numpy.savetxt("testsimulation.csv", APs_wot, delimiter=",", fmt = '%d')
pickle.dump(APs_wot, open("test_simulation", "wb"))

rectime = stop-start
numelectrodes = 12
timeBin = 25 * ms  # timebin to compute spikerate
APsinbin = zeros((numelectrodes, int(floor(rectime / timeBin))))
spikerate = zeros((numelectrodes, int(floor(rectime / timeBin))))

# Compute spikerate
for l in range(numelectrodes):
    APt = APs_wot[APs_wot[:, 0] == l, :]  # fo through one electrode first
    APt = APt[:, 1]  # take only the timestamps
    binsize = timeBin * fs / second
    for k in range(int(floor(rectime/ timeBin))):
        APsinbin[l, k] = sum((APt > (k * binsize)) & (APt < ((k + 1) * binsize)))
        spikerate[l, k] = APsinbin[l, k] * second / timeBin

APsinbintot = sum(APsinbin, axis=0)
spikeratetot = sum(spikerate, axis=0)

spikeratetime = arange(0, len(spikeratetot) * timeBin / second, timeBin / second)

# smooth the firing rate using a gaussian kernel
def gaussian_kernel(width, sigma):
    x = np.arange(0, width, 1, float)
    x = x - width // 2
    return norm.pdf(x, scale=sigma)

kernel_size = 11
sigma = 3.0
kernel = gaussian_kernel(kernel_size, sigma)
kernel /= np.sum(kernel)
spikeratesmooth = np.convolve(spikeratetot, kernel, mode='same')

# detect fragments
peaks, ph = find_peaks(spikeratesmooth, height = (1/16)*max(spikeratetot), prominence = (1/10)*max(spikeratetot))

if firingrateplot:
    figure(figsize = (8, 3), dpi=200)
    plot(spikeratetime, spikeratesmooth, 'k')
    scatter(spikeratetime[peaks], ph['peak_heights'], s = 50)
    xlabel('time (second)')
    ylabel('spikerate (Hz)')
    plt.show()
