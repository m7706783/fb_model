# Script to analyze experimental or simulated neuronal network activity for Fragmented Burst detection # Doorn et el. 2024

# Written by Nina Doorn
# last edit: 19-02-2024
# n.doorn-1@utwente.nl

import scipy.io as sio
from brian2 import *
from scipy.stats import norm
import numpy as np
from scipy.signal import find_peaks
import pickle

# Load experimental data
APs_C = sio.loadmat('/home/Nina/Documents/FB_project/APs/FB2_final/APS_FB2_B6.mat')
APs = APs_C['Ts_AP']

# Or load simulated data
#APs = pickle.load(open("APs_Dyn3sim2002_realblock", "rb"))

fs = 10000                          # sampling frequency or 1/dt

# Take only the wanted time interval of the measurement
start = 0 * second
stop = 300 * second
APs_wot = APs[APs[:, 1] > start * fs / second, :]
APs_wot = APs[APs[:, 1] < stop * fs / second, :]
APs_wot[:, 1] = APs_wot[:, 1] - start * fs / second

rectime = stop-start

# Compute the firing rate
numelectrodes = 12
timeBin = 25 * ms  # timebin to compute spikerate
APsinbin = zeros((numelectrodes, int(floor(rectime / timeBin))))
spikerate = zeros((numelectrodes, int(floor(rectime / timeBin))))

# Compute spikerate
for l in range(numelectrodes):
    APt = APs_wot[APs_wot[:, 0] == l, :]  # fo through one electrode first
    APt = APt[:, 1]  # take only the timestamps
    binsize = timeBin * fs / second
    for k in range(int(floor(rectime/ timeBin))):
        APsinbin[l, k] = sum((APt > (k * binsize)) & (APt < ((k + 1) * binsize)))
        spikerate[l, k] = APsinbin[l, k] * second / timeBin

APsinbintot = sum(APsinbin, axis=0)
spikeratetot = sum(spikerate, axis=0)

# Smooth the firing rate with a Gaussian kernel
def gaussian_kernel(width, sigma):
    x = np.arange(0, width, 1, float)
    x = x - width // 2
    return norm.pdf(x, scale=sigma)

kernel_size = 13
sigma = 4.0
kernel = gaussian_kernel(kernel_size, sigma)
kernel /= np.sum(kernel)
spikeratesmooth = np.convolve(spikeratetot, kernel, mode='same')

# Detect network bursts on spikerate
actelec = sum(mean(spikerate, axis=1) > 0.02)  # calculate the number of active electrodes
Startth = 0.25 * max(spikeratetot)  # threshold to start a burst
Tth = int((50 * ms) / timeBin)  # how long it has to surpass threshold
Eth = 0.3 * actelec  # active electrode number threshold
MBth = (1 / 16) * max(spikeratetot)  # threshold to detect miniburst
Stopth = (1 / 50) * max(spikeratetot)  # threshold to stop a burst

peaks, ph = find_peaks(spikeratesmooth, height = MBth, prominence = (1/10)*max(spikeratetot))

# initialize
i = 0
NBcount = 0
maxNB = 1000
NBs = zeros((maxNB, 5))
spikeratetot = spikeratesmooth
movingint = int(30 * second / timeBin)              # Compute the detection threshold every 30 seconds to account for dynasore effect variability
Startths = zeros(len(spikeratetot))
Stoptths = zeros(len(spikeratetot))

# detect NBs
while (i + Tth) < len(spikeratetot):
    if (i > movingint) & (i < len(spikeratetot) - movingint):
        Startth = 0.25 * max(spikeratetot[i - movingint:i + movingint])  # threshold to start a burst
        Stopth = (1 / 100) * max(spikeratetot[i - movingint:i + movingint])  # threshold to stop a burst
    elif (i < len(spikeratetot) - movingint):
        Startth = 0.25 * max(spikeratetot[0:i + movingint])  # threshold to start a burst
        Stopth = (1 / 100) * max(spikeratetot[0:i + movingint])  # threshold to stop a burst
    else:
        Startth = 0.25 * max(spikeratetot[i - movingint: len(spikeratetot)])  # threshold to start a burst
        Stopth = (1 / 100) * max(spikeratetot[i - movingint: len(spikeratetot)])  # threshold to stop a burst
    Startths[i] = Startth
    Stoptths[i] = Stopth
    if (all(spikeratetot[i:i + Tth] > Startth)) \
            & (sum(sum(APsinbin[:, i:i + Tth], axis=1) > Tth) > Eth):
        NBs[NBcount, 2] = NBs[NBcount, 2] + sum(APsinbintot[i:i + Tth])
        NBs[NBcount, 0] = i
        i = i + Tth
        while any(spikeratetot[i:i + 2 * Tth] > Stopth):
            NBs[NBcount, 2] = NBs[NBcount, 2] + APsinbintot[i]
            i = i + 1
        NBs[NBcount, 4] = sum((peaks > NBs[NBcount, 0]) & (peaks < i))
        NBs[NBcount, 1] = i
        NBcount = NBcount + 1
    else:
        i = i + 1

NBs = NBs[0:NBcount, :]

# define measures based on network bursts
MNBR = NBcount * 60 * second / rectime # Average network burst rate
NBdurations = (array(NBs[:, 1]) - array(NBs[:, 0])) * timeBin / second  # NB durations
MNBD = mean(NBdurations)    # Average NB duration
PSIB = sum(NBs[:, 2] / len(APs_wot)) * 100  # Percentage of Spikes In network Bursts
MFR = len(APs_wot) / (rectime / second) / numelectrodes  # Mean firing rate
IBI = (array(NBs[1:, 0]) - array(NBs[0:-1, 1])) * timeBin / second  # Inter Bursts intervals
CVIBI = np.std(IBI) / np.mean(IBI)  # Coefficient of variation of the interburst intervals
NFBs = sum(NBs[:,4])/NBcount            # Average number of fragments per NB

print('MFR= ',MFR, '\nNBR= ', MNBR, '\nNBD= ', MNBD, '\nPSIB= ', PSIB, '\nCVIBI= ', CVIBI, '\n#FBs= ', NFBs)
