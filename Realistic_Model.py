# Computational model used for studying fragmented network bursts in Doorn et al. 2024.
# Model of neuronal network derived from hiPSCs on MEA (Doorn et al. 2023)
# Using a HH-type model including a slow after-hyperpolarization current and membrane noise (Masquelier, Deco 2013).
# Connected into a network with N neurons, with random sparse connectivity
# Connected via AMPAr and NMDAr mediated synapses, subjected to short-term plasticity (Masquelier, Deco, 2013).
# Choice of additional asynchronous neurotransmitter release (Wang et al. 2016)
# Choice of distributed weights and distant dependent conduction delays. (Doorn et al. 2023)
# Virtual electrodes are placed to mimic experimental data (Doorn et al. 2023)

# Written by Nina Doorn
# last edit: 19-02-2024
# n.doorn-1@utwente.nl

# IMPORT LIBRARIES
from brian2 import devices
import scipy
from scipy import signal
import os
from numpy import *
from brian2 import *
import networkx as nx
import matplotlib.pyplot as plt
import pickle
from scipy.signal import find_peaks
from scipy.io import savemat
from scipy.stats import norm
import numpy as np

# # Skip C99 support check
import brian2.codegen.cpp_prefs
brian2.codegen.cpp_prefs._compiler_supports_c99 = True
BrianLogger.suppress_hierarchy('brian2.devices')
BrianLogger.suppress_hierarchy('brian2.parsing')

# choose with plots you want to see
voltplot = True                     # plots the membrane potential of one neuron in the network over time
electrodeplot = True                # plots the voltage measured by the 12 virtual electrodes
rasterplot = True                   # Makes a brian2 rasterplot with all the neurons
rasterlecplot = True                # Makes a rasterplot of APs detected at electrodes (electrodeplot should be True)
STDplot = True                      # Plots short-term depression values over time
synapseplot = True                  # Plots AMPA and NMDA currents over time
adaptationplot = True               # Plots the sAHP current over time
singlechannelplot = True            # Plots the voltage at a single "MEA"-electrode
mechanismplot = False               # Plots the mechanisms responsible for (fragmented) NB generation
Asynchronousplot = True             # Plots the asynchronous release rate over time
firingrateplot = True               # Plots the network firing rate with detected fragments over time

# SET PARAMETERS
# simulation parameters
simtime = 65 * second               # simulation time
transient = 5 * second              # time omitted as transient
sed = 39                             # random number seed
devices.device.seed(sed)            # set the seed for all the random number realisations

outputdir = '/home/Nina/Documents/FB_project/Output'    # directory to save figures
simname = '/test_'                                      # simulation name to save figures

# network parameters
Nl = 10
N = Nl * Nl                         # number of neurons
connprob = 0.2                      # connection probability between neurons

distributed = True                  # Choose if the synaptic weights are normally distributed with standard deviation sd
sd = 0.7
DistDelays = True                   # Choose if the delays should be distant dependent
Maxdelay = 25*ms                    # with maximum conduction delay

# neuron parameters
area = 300*umetre**2                # membrane area of the neuron
Cm = (2*ufarad*cm**-2) * area       # membrane capacitance
El = -39.2 * mV                     # Nernst potential of leaky ions
EK = -80 * mV                       # Nernst potential of potassium
ENa = 70 * mV                       # Nernst potential of sodium
g_na = 1.6 * 50 * msiemens * cm ** -2 * area  # maximal conductance of sodium channels
g_kd = 1.3 * 5 * msiemens * cm ** -2 * area   # maximal conductance of potassium
gl = (0.3*msiemens*cm**-2) * area   # maximal leak conductance
VT = -30.4*mV                       # alters firing threshold of neurons
sigma = 6 * mV                      # standard deviation of the noisy voltage fluctuations

# Adaptation parameters
E_AHP = EK                          # Nernst potential of afterhyperpolarization current
g_AHP = 5 * nS                      # Maximum conductance of sAHP channels
tau_Ca = 8000 * ms                  # recovery time constant sAHP channels
alpha_Ca = 0.00035                  # strength of the spike-frequency adaptation

# synapse parameters
S = 0.4                             # Overall synaptic strength multiplicative factor
delta = 0.6                         # changes NMDAR/AMPAR ratio, should be between -1 and 1
g_ampa = (1 + delta) * nS           # maximal conductance of AMPA channels
g_nmda = (1 - delta) * nS           # maximal conductance of NMDA channels
E_ampa = 0 * mV                     # Nernst potentials of synaptic channels
E_nmda = 0 * mV
tau_ampa = 2 * ms                   # recovery time constant of AMPA conductance
taus_nmda = 100 * ms                # rise time constant of NMDA conductance
taux_nmda = 2 * ms                  # decay time constant of NMDA conductance
alpha_nmda = 0.5 * kHz

tau_d = 200 * ms                    # Recovery time constant of short-term synaptic depression (STD)
U = 0.2                             # Strength of STD

STF = False                         # Whether to include Short-term synaptic facilitation (STF)
tau_f = 1000 * ms                   # Recovery time constant of STF

AsynchronousRelease = False         # Whether to include asynchronous neurotransmitter release
tau_ar = 700 * ms                   # Recovery time constant of asynchronous release
Uar = 0.003                         # increment of asynchronous release probability induced by an action potential
Umax = 0.5/ms                       # Saturation level of facilitation
x0 = 5                              # quantum size

# BUILD NETWORK
# neuron model
eqs = Equations('''
dV/dt = noise + (-gl*(V-El)-g_na*(m*m*m)*h*(V-ENa)-g_kd*(n*n*n*n)*(V-EK)+I-I_syn+I_AHP)/Cm : volt
dm/dt = alpha_m*(1-m)-beta_m*m : 1
dh/dt = alpha_h*(1-h)-beta_h*h : 1
dn/dt = (alpha_n*(1-n)-beta_n*n) : 1
dhp/dt = 0.128*exp((17.*mV-V+VT)/(18.*mV))/ms*(1.-hp)-4./(1+exp((30.*mV-V+VT)/(5.*mV)))/ms*h : 1
alpha_m = 0.32*(mV**-1)*4*mV/exprel((13*mV-V+VT)/(4*mV))/ms : Hz
beta_m = 0.28*(mV**-1)*5*mV/exprel((V-VT-40*mV)/(5*mV))/ms : Hz
alpha_h = 0.128*exp((17*mV-V+VT)/(18*mV))/ms : Hz
beta_h = 4./(1+exp((40*mV-V+VT)/(5*mV)))/ms : Hz
alpha_n = 0.032*(mV**-1)*5*mV/exprel((15*mV-V+VT)/(5*mV))/ms : Hz
beta_n = .5*exp((10*mV-V+VT)/(40*mV))/ms : Hz
noise = sigma*(2*gl/Cm)**.5*randn()/sqrt(dt) :volt/second (constant over dt)
I : amp
I_syn =  I_ampa + I_nmda: amp
I_ampa = g_ampa*(V-E_ampa)*(s_ampa) : amp
I_nmda = g_nmda*(V-E_nmda)*(s_nmda_tot)/(1+exp(-0.062*V/mV)/3.57) : amp
s_nmda_tot :1
ds_ampa/dt = -s_ampa/tau_ampa + qar_tot :1 
qar_tot : Hz
I_AHP = -g_AHP*Ca*(V-E_AHP) : amp
dCa/dt = - Ca / tau_Ca : 1 
x : meter
y : meter
''')


# Make population of neurons
P = NeuronGroup(N, model=eqs, threshold='V>0*mV', reset='Ca += alpha_Ca', refractory=2 * ms,
                    method='exponential_euler')

# Initialize neuron parameters
P.V = -39 * mV                          # approximately resting membrane potential
P.I = '(rand() -0.5) * 19 * pA'          # Make neurons heterogeneously excitable

# Position neurons on a grid
grid_dist = 45 * umeter
P.x = '(i % Nl) * grid_dist'
P.y = '(i // Nl) * grid_dist'

# synapse model
if AsynchronousRelease:
    eqs_synapsmodel = '''
    s_nmda_tot_post = w * S * s_nmda * x_d :1 (summed) 
    qar_tot_post = w * S * x0 * qar :Hz (summed)
    qar = clip(randn()*sqrt(x_d/x0*uar*dt*(1-uar*dt))+uar*dt*x_d/x0, 0, 2*x_d/x0*uar*dt)/dt :Hz (constant over dt)
    ds_nmda/dt = -s_nmda/(taus_nmda)+alpha_nmda*(x_nmda)*(1-s_nmda) + x0 * qar : 1 (clock-driven)
    dx_nmda/dt = -x_nmda/(taux_nmda) :1 (clock-driven)
    dx_d/dt = (1-x_d)/tau_d -qar :1 (clock-driven)
    duar/dt = -uar/tau_ar :Hz (clock-driven)
    w : 1
    '''
    eqs_onpre = '''
    x_nmda += 1 
    x_d *= (1-U) 
    uar += Uar*(Umax-uar)
    s_ampa += w * S * x_d 
    '''
elif STF:
    eqs_synapsmodel = '''
    s_nmda_tot_post = w * S * x_d * u_d * s_nmda  :1 (summed)
    ds_nmda/dt = -s_nmda/(taus_nmda)+alpha_nmda*x_nmda*(1-s_nmda) : 1 (clock-driven)
    dx_nmda/dt = -x_nmda/(taux_nmda) :1 (clock-driven)
    dx_d/dt = (1-x_d)/tau_d :1 (clock-driven)
    du_d/dt = -u_d/tau_f :1 (clock-driven)
    w : 1
    '''
    eqs_onpre = '''
    x_nmda += 1
    x_d *= (1-u_d)
    u_d += U*(1-u_d)
    s_ampa += w * S * x_d * u_d
    '''
else:
    eqs_synapsmodel = '''
    s_nmda_tot_post = w * S * x_d * s_nmda  :1 (summed)
    ds_nmda/dt = -s_nmda/(taus_nmda)+alpha_nmda*x_nmda*(1-s_nmda) : 1 (clock-driven)
    dx_nmda/dt = -x_nmda/(taux_nmda) :1 (clock-driven)
    dx_d/dt = (1-x_d)/tau_d :1 (clock-driven)
    w : 1
    '''
    eqs_onpre = '''
    x_nmda += 1
    x_d *= (1-U)
    s_ampa += w * S * x_d 
    '''

# Make synapses
Conn = Synapses(P, P, model=eqs_synapsmodel, on_pre=eqs_onpre, method='euler')

# connect neurons
Conn.connect(p=connprob)

if distributed:
    Conn.w[:] = 'clip(1.+sd*randn(), 0, 2)'
else:
    Conn.w[:] = 1

Conn.x_d[:] = 1

if DistDelays:
    Vmax = (sqrt(Nl ** 2 + Nl ** 2) * grid_dist) / Maxdelay
    Conn.delay = '(sqrt((x_pre - x_post)**2 + (y_pre - y_post)**2))/Vmax'
else:
    Conn.delay = Maxdelay

# SET UP MONITORS AND RUN
recordstring = ['V']
if synapseplot or STDplot or mechanismplot:
    recordstring.extend(['I_ampa', 'I_nmda', 'I_syn', 'qar_tot'])

if adaptationplot or STDplot or mechanismplot:
    recordstring.append('I_AHP')

dt2 = defaultclock.dt                                           # Allows for chanching the timestep of recording

trace = StateMonitor(P, recordstring, record=True, dt=dt2)

import random
recordlist = random.sample(range(1, len(Conn)-1), 100)          # Only measure a subset of synapses for speed
if STDplot or mechanismplot:
    syntrace = StateMonitor(Conn, ['x_d'], record = recordlist, dt=dt2)

spikes = SpikeMonitor(P)

run(simtime, report='text', profile=True)

# PLOTS
if voltplot:
    plt.figure(dpi=200)
    plot(trace.t / second, trace[2].V / mV, 'k', linewidth=0.7)
    xlabel('time (s)')
    ylabel('Membrane Potential of a neurons (mV)')
    xlim([21, 22.5])
    savefig(outputdir + simname + 'voltsingleneuron.png')
    show()

if rasterplot:
    plt.figure(dpi=200)
    plt.plot(spikes.t / second, spikes.i, '.k', ms=0.7)
    xlabel('time (s)')
    ylabel('neuron index')
    #xlim([45, 49])
    #xlim([transient/second, simtime/second])
    savefig(outputdir + simname + 'brianraster.png')
    show()

# set up a filter to filter the voltage signal
fs = 1 / (dt2 / second)
fc = 100                                            # Cut-off frequency of the filter
w = fc / (fs / 2)                                   # Normalize the frequency
b, a = signal.butter(2, w, 'high')
voltagetraces = zeros((12,len(trace.t)))

if electrodeplot:
    elec_grid_dist = (grid_dist * (Nl - 1)) / 4     # electrode grid size (there are 12 electrodes)
    elec_range = 3 * grid_dist                      # measurement range of each electrode
    comp_dist = ((Nl - 1) * grid_dist - elec_grid_dist * 3) / 2

    try:
        elecranges = pickle.load(open("elecranges.dat", "rb"))
    except FileNotFoundError:
        #Find the neurons that need to be measures, outcomment the first time run.
        elecranges = np.zeros((16, 40), dtype=int)
        for i in range(len(elecranges)):
            templist = list()
            for j in range(N):
                x_electrode = i % 4 * elec_grid_dist + comp_dist
                y_electrode = i // 4 * elec_grid_dist + comp_dist
                if sqrt((x_electrode - P[j].x) ** 2 + (y_electrode - P[j].y) ** 2) < elec_range:
                    templist.append(j)
                elecranges[i, 0:len(templist)] = templist
            templist = None
        pickle.dump(elecranges, open("elecranges.dat", "wb"))

    plt.figure(figsize=(10, 6), dpi=100)
    k = 0
    MaxAPs = len(spikes.t)
    APs = np.array([0,0])
    for i in [1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14]:
        templist = elecranges[i, :]
        x_electrode = i % 4 * elec_grid_dist + comp_dist
        y_electrode = i // 4 * elec_grid_dist + comp_dist
        templist = [j for j in templist if j != 0]
        Voltage = np.zeros(len(trace.V[0]))
        for l in range(len(templist)):
            Voltage += trace[templist[l]].V / mV * 1 / (
                        sqrt((x_electrode - P.x[templist[l]]) ** 2 + (y_electrode - P.y[templist[l]]) ** 2) / (
                            grid_dist * 0.2))
        Voltage = Voltage - mean(Voltage)
        Voltagefilt = signal.filtfilt(b, a, Voltage)  # high pass filter
        threshold = 4 * np.sqrt(np.mean(Voltagefilt ** 2))      #threshold to detect APs
        APstemp, _ = find_peaks(abs(Voltagefilt), height=threshold)
        for j in range(len(APstemp)):
            APs = np.append(APs, [k,APstemp[j]])
        voltagetraces[k, :] = Voltagefilt
        plt.plot(trace.t / second, Voltagefilt + k * 100, linewidth=0.75, color='black')
        templist = None
        k += 1
    xlabel('time (s)', fontsize=15)
    plt.tick_params(
        axis='y',
        which='both',
        left=False,
        right=False,
        labelleft=False)
    #xlim([transient/second,simtime/second])
    tight_layout()
    plt.savefig(outputdir + simname + 'elecvolt.png')
    show()


if rasterlecplot:
    APsres = APs.reshape(len(APs) // 2, 2)
    figure(figsize=(10, 2.2), dpi=300)
    plt.plot(APsres[:, 1] * dt2, APsres[:, 0], 'k|', ms=9, alpha=.5)
    plt.hlines([-0.3, 0.7, 1.7, 2.7, 3.7, 4.7, 5.7, 6.7, 7.7, 8.7, 9.7, 10.7], -3, simtime/second+3, colors='black',
               linewidths=0.5)
    xlabel('Time (second)')
    ylabel('Spike times per electrode')
    xlim([transient/second-1, simtime/second+1])
    #xlim([19, 24])
    axis("off")
    tight_layout()
    savefig(outputdir + simname + 'ownraster.png')
    show()

if STDplot:
    fig, axs = plt.subplots(4, 1, sharex='all', dpi=200, figsize=[8, 6.5])
    axs[0].plot(spikes.t / second, spikes.i, '.k', ms=1)
    axs[0].set_ylabel('Neuron index')

    axs[1].plot(trace.t / second, sum(trace.I_nmda + trace.I_ampa, axis=0) / len(trace.I_nmda) * -1 / pA, 'k')
    axs[1].set_ylabel('Synaptic current (pA)')
    axs[1].grid(visible=True, linestyle=':')

    axs[2].plot(trace.t / second, sum(syntrace.x_d, axis=0) / len(syntrace.x_d) * U, 'k')
    axs[2].set_ylabel('STD')
    axs[2].grid(visible=True, linestyle=':')

    axs[3].plot(trace.t / second, sum(trace.I_AHP, axis=0) / len(trace.I_AHP) * -1 / pA, 'k')
    axs[3].set_ylabel('sAHP-current (pA)')
    axs[3].set_xlabel('time (s)')
    axs[3].grid(visible=True, linestyle=':')
    fig.tight_layout()
    xlim([17.5, 20])
    savefig(outputdir + simname + 'STDplot.png', dpi=300)
    show()

if synapseplot:
    fig, axs = plt.subplots(2, 1, sharex='all', dpi=200, figsize=[8, 5])
    axs[0].plot(trace.t / second, sum(trace.I_nmda, axis=0) / len(trace.I_nmda) * -1 / pA, 'k')
    axs[0].set_ylabel('NMDA current (pA)')
    axs[0].grid(b=True, linestyle=':')

    axs[1].plot(trace.t / second, sum(trace.I_ampa, axis=0) / len(trace.I_ampa) * -1 / pA, 'k')
    axs[1].set_ylabel('AMPA current (pA)')
    axs[1].set_xlabel('time (s)')
    axs[1].grid(b=True, linestyle=':')
    fig.tight_layout()
    #xlim([0,1])
    savefig(outputdir + simname + 'EPSPs.png', dpi=300)
    show()

if adaptationplot:
    plot(trace.t / ms, sum(trace.I_AHP, axis=0) / len(trace.I_AHP)/pA)
    xlabel('t (ms)')
    ylabel('I_AHP (pA)')
    show()

if singlechannelplot:
    figure(dpi=300)
    plot(trace.t / second, -voltagetraces[4, :], 'k', linewidth=0.75)
    xlabel('time (s)')
    ylabel('Voltage (mV)')
    xlim([29, 54])
    #ylim([-50, 80])
    savefig(outputdir + simname + 'onelecvolt.png')
    show()

if mechanismplot:
    fig, axs = plt.subplots(4, 1, sharex='all', dpi=200, figsize=[5, 3.5])
    axs[0].scatter(spikes.t / second, spikes.i, s=0.01, c='k')
    axs[0].set_ylabel('Neuron\n index')

    axs[2].plot(trace.t / second, sum(trace.I_syn, axis=0) / len(trace.I_syn) * -1 / pA, 'k', linewidth=1)
    axs[2].set_ylabel('Synaptic\n Cur (pA)')
    axs[2].grid(visible=True, linestyle=':')

    axs[1].plot(trace.t / second, sum(syntrace.x_d, axis=0) / len(syntrace.x_d), 'k', linewidth=1)
    if STF:
        axs[1].plot(trace.t / second, sum(syntrace.u_d, axis=0) / len(syntrace.u_d), 'royalblue')
    axs[1].set_ylabel('STD')
    if STF:
        axs[1].legend(['x', 'u'])
    axs[1].grid(visible=True, linestyle=':')

    axs[3].plot(trace.t / second, sum(trace.I_AHP, axis=0) / len(trace.I_AHP) * -1 / pA, 'k', linewidth=1)
    axs[3].set_ylabel('Adaptive\n Cur (pA)')
    axs[3].set_xlabel('Time (s)')
    axs[3].grid(visible=True, linestyle=':')
    fig.tight_layout()
    xlim([transient/second, (simtime-transient)/second])
    xlim([30.5, 40.5])
    savefig(outputdir + simname + 'mechplot.svg', bbox_inches='tight', transparent=True,dpi=300,format="svg")
    show()

if Asynchronousplot:
    fig, axs = plt.subplots(2, 1, sharex='all', dpi=200, figsize=[4, 4])

    axs[0].plot(trace.t / second, sum(trace.qar_tot, axis=0) / len(trace.qar_tot) * second, 'k', linewidth=1)
    axs[0].set_ylabel('Asyn rate (Hz)')
    axs[0].grid(visible=True, linestyle=':')

    axs[1].plot(trace.t / second, sum(syntrace.x_d, axis=0) / len(syntrace.x_d), 'k', linewidth=1)
    axs[1].set_ylabel('STD')
    axs[1].grid(visible=True, linestyle=':')
    axs[1].set_xlabel('Time (s)')

    fig.tight_layout()
    xlim([transient/second, (simtime-transient)/second])
    savefig(outputdir + simname + 'asynplot.svg', bbox_inches='tight', transparent=True,dpi=300,format="svg")
    show()

# CALCULATE NETWORK FIRING RATE
APs = APs.reshape(len(APs) // 2, 2)

# remove transient or select certain area
start = transient
stop = simtime
APs_wot = APs[APs[:, 1] > start * fs / second, :]
APs_wot = APs_wot[APs_wot[:, 1] < stop * fs / second, :]
APs_wot[:, 1] = APs_wot[:, 1] - start * fs / second

#Maybe save the data for analysis
numpy.savetxt("testsimulation.csv", APs_wot, delimiter=",", fmt = '%d')
pickle.dump(APs_wot, open("test_simulation", "wb"))

rectime = stop-start
numelectrodes = 12
timeBin = 25 * ms  # timebin to compute spikerate
APsinbin = zeros((numelectrodes, int(floor(rectime / timeBin))))
spikerate = zeros((numelectrodes, int(floor(rectime / timeBin))))

# Compute spikerate
for l in range(numelectrodes):
    APt = APs_wot[APs_wot[:, 0] == l, :]  # fo through one electrode first
    APt = APt[:, 1]  # take only the timestamps
    binsize = timeBin * fs / second
    for k in range(int(floor(rectime/ timeBin))):
        APsinbin[l, k] = sum((APt > (k * binsize)) & (APt < ((k + 1) * binsize)))
        spikerate[l, k] = APsinbin[l, k] * second / timeBin

APsinbintot = sum(APsinbin, axis=0)
spikeratetot = sum(spikerate, axis=0)

spikeratetime = arange(0, len(spikeratetot) * timeBin / second, timeBin / second)

# smooth the firing rate using a gaussian kernel
def gaussian_kernel(width, sigma):
    x = np.arange(0, width, 1, float)
    x = x - width // 2
    return norm.pdf(x, scale=sigma)

kernel_size = 11
sigma = 3.0
kernel = gaussian_kernel(kernel_size, sigma)
kernel /= np.sum(kernel)
spikeratesmooth = np.convolve(spikeratetot, kernel, mode='same')

# detect fragments
peaks, ph = find_peaks(spikeratesmooth, height = (1/16)*max(spikeratetot), prominence = (1/10)*max(spikeratetot))

if firingrateplot:
    figure(figsize = (8, 3), dpi=200)
    plot(spikeratetime, spikeratesmooth, 'k')
    scatter(spikeratetime[peaks], ph['peak_heights'], s = 50)
    xlabel('time (second)')
    ylabel('spikerate (Hz)')
    plt.show()

