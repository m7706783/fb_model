# FB_model

All code and files used to reproduce the original results in Doorn et al. 2024: Breaking the Burst: Origins of Fragmented Network Bursts in Patient-derived Neurons Revealed

Preprint van be found here: https://www.biorxiv.org/content/10.1101/2024.02.28.582501v1

## Computational models
The realistic computational model is largely based on the work described in Doorn et al. 2023 (https://www.sciencedirect.com/science/article/pii/S2213671123002321?via%3Dihub). 

The minimal model is a simpler version of the realistic model, containing 100 identical EIF neurons instead of 100 heterogeneous HH neurons, and simple single exponential synapses without conduction delays or variable synaptic weights. 

Both models are simulated using the brian2 simulator: https://brian2.readthedocs.io/en/stable/

## Data analysis
There is also a python script with the algorithm to detect Network Bursts (NBs) and fragments in peaktrains (1nd column with electrode number and 2nd column with timepoint of the spike detected at that electrode) that are obtained either experimentally or through simulations. 

## Data
The provided experimental and simulated peaktrains are used to obtain the Dynasore results of Figure 3 of the paper. Time stamps used are given in Supplemental Table S2 of the paper. 

